**TODO**

**AXIS_ANGLE**<br>
A-Field DONE<br>
A-Constr DONE<br>
A-Ide DONE<br>
A-App DONE - via Matrix<br>
A-FromPoint DONE<br>
A-Ax a DONE - via Matrix<br>
A-Ax b DONE - via Matrix<br>
A-Ax c DONE - via Matrix<br>
A-operator* DONE - via Matrix<br>
A-Inv a DONE<br>
A-Inv b DONE<br>
A-LookAt DONE - via Matrix<br>
A-ToFrom DONE<br>
A-Rx DONE<br>
A-Ry DONE<br>
A-Rz DONE<br>
E2A DONE - via Matrix<br>
Q2A DONE<br>
M2A DONE<br>
A-isP DONE<br>
Print DONE<br>
A-Lerp DONE<br>

**EULER**<br>
E-Field DONE<br>
E-Constr DONE<br>
E-Ide DONE<br>
E-App DONE - via Matrix<br>
E-Ax a DONE - via Matrix<br>
E-Ax b DONE - via Matrix<br>
E-Ax c DONE - via Matrix<br>
E-operator* DONE - via Matrix<br>
E-Inv a DONE<br>
E-Inv b DONE<br>
E-LookAt DONE - via Matrix<br>
E-ToFrom DONE - via Matrix<br>
E-Rx DONE<br>
E-Ry DONE<br>
E-Rz DONE<br>
A2E DONE - via Quaternion<br>
Q2E DONE - via Matrix<br>
M2E DONE<br>
E-isR DONE<br>
E-Print DONE<br>
E-DirectLerp DONE<br>
E-Lerp DONE - via Quaternion (ovviamente)<br>

**QUATERNION**<br>
Q-Field DONE<br>
Q-Constr DONE<br>
Q-Ide DONE<br>
Q-App DONE<br>
Q-FromPoint DONE<br>
Q-Ax a DONE<br>
Q-Ax b DONE<br>
Q-Ax c DONE<br>
Q-operator* DONE<br>
Q-Inv a DONE<br>
Q-Inv b DONE<br>
Q-Conj a DONE<br>
Q-Conj b DONE<br>
Q-LookAt DONE - via Matrix<br>
Q-ToFrom DONE<br>
Q-Rx DONE - via AxisAngle<br>
Q-Ry DONE - via AxisAngle<br>
Q-Rz DONE - via AxisAngle<br>
A2Q DONE<br>
E2Q DONE<br>
M2Q DONE - via AxisAngle<br>
Q-isP DONE<br>
Q-isR DONE<br>
Q-Print DONE<br>
Q-Lerp DONE<br>

**MATRIX3**<br>
M-Field DONE<br>
M-Constr DONE<br>
M-Ide DONE<br>
M-App DONE<br>
M-Ax a DONE<br>
M-Ax b DONE<br>
M-Ax c DONE<br>
M-operator* DONE<br>
M-Inv a DONE<br>
M-Inv b DONE<br>
M-Transpose a DONE<br>
M-Transpose b DONE<br>
M-LookAt DONE<br>
M-ToFrom DONE<br>
M-Rx DONE<br>
M-Ry DONE<br>
M-Rz DONE<br>
A2M DONE<br>
E2M DONE<br>
Q2M DONE<br>
M-isR DONE<br>
M-Print DONE<br>
M-DirectLerp DONE<br>
M-Lerp DONE - via Quaternion (ovviamente)<br>

**VECTOR3**<br>
Print DONE<br>

**POINT3**<br>
Print DONE<br>

**VERSOR3**<br>
nlerp DONE<br>
slerp DONE<br>
asVersor DONE<br>
Print DONE<br>

**TRANSFORM**<br>
T-place DONE<br>
T-invert DONE<br>
T-DrectXMatrix<br>
T-lerp<br>

**PROBLEMS**<br>
P-inter-plane DONE<br>
P-isSeen DONE<br>
P-reflect DONE<br>
P-coplanar DONE<br>
P-planeNorm DONE<br>
P-ortogonalize DONE<br>
P-hitPos DONE
