#pragma once
#include <math.h>
#include "vector3.h"
#include "point3.h"
#include "versor3.h"

/* AxisAngle class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

class Quaternion;
class Euler;
class Matrix3;

class AxisAngle{
public:
    Versor3 axis;
    Scalar angle;

	AxisAngle();
    AxisAngle(const Point3& axis);
    AxisAngle(const Versor3& axis);
    AxisAngle(const Vector3& axis);
	AxisAngle(const Point3& axis, Scalar angle);
	AxisAngle(const Versor3& axis, Scalar angle);
	AxisAngle(const Vector3& axis, Scalar angle);



    Vector3 apply( Vector3  v) const;

    // Rotations can be applied to versors or vectors just as well
    Versor3 apply( Versor3 dir ) const;

    Point3 apply( Point3 p ) const;

    // syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
    Versor3 operator() (Versor3 p);
    Point3 operator() (Point3 p);
    Vector3 operator() (Vector3 p);

    Versor3 axisX() const;  // TODO A-Ax a
    Versor3 axisY() const;  // TODO A-Ax b
    Versor3 axisZ() const;  // TODO A-Ax c

    // conjugate
    AxisAngle operator * (AxisAngle r) const;

    AxisAngle inverse() const;

    void invert();

    // returns a rotation to look toward target, if you are in eye, and the up-vector is up
    static AxisAngle lookAt( Point3 eye, Point3 target, Versor3 up = Versor3::up() );

    // returns a rotation
    static AxisAngle toFrom( Versor3 to, Versor3 from );

    static AxisAngle toFrom( Vector3 to, Vector3 from );

    // conversions to this representation
    static AxisAngle from( Matrix3 m );   // TODO M2A
    static AxisAngle from( Euler e );     // TODO E2A
    static AxisAngle from( Quaternion q );// TODO Q2A

    bool isRot() const;

    // does this AxisAngle encode a point?
    bool isPoint() const;

	static AxisAngle rotationX(Scalar angleDeg);
	static AxisAngle rotationY(Scalar angleDeg);
	static AxisAngle rotationZ(Scalar angleDeg);

    void printf() const;
};


// interpolation or roations
inline AxisAngle lerp(const AxisAngle& a, const AxisAngle& b, Scalar t) {
    return AxisAngle(slerp(a.axis, b.axis, t), (1 - t) * a.angle + t * b.angle);

    //Vector3 res = lerp(a.axis * a.angle, b.axis * b.angle, t);
    //Scalar n = norm(res);
	//return AxisAngle(res/n, n);
    //slerp axis lerp angle
}