#pragma once

#include <math.h>
#include "quaternion.h"
#include "euler.h"
#include "axis_angle.h"
#include "matrix3.h"

// TODO M-Ide: this constructor construct the identity rotation
Matrix3::Matrix3():x(Versor3::right()), y(Versor3::up()), z(Versor3::forward()) { }

Matrix3::Matrix3(Versor3 _x, Versor3 _y, Versor3 _z) :x(_x), y(_y), z(_z) { }
Matrix3::Matrix3(Vector3 _x, Vector3 _y, Vector3 _z) : x(normalize(_x)), y(normalize(_y)), z(normalize(_z)) { }

// constructor that takes as input the coefficient (RAW-MAJOR order!)
Matrix3::Matrix3(Scalar m00, Scalar m01, Scalar m02,
	Scalar m10, Scalar m11, Scalar m12,
	Scalar m20, Scalar m21, Scalar m22):
	Matrix3(Vector3(m00, m01, m02), Vector3(m10, m11, m12), Vector3(m20, m21, m22)) { }

Vector3 Matrix3::apply(Vector3  v) const {
	// TODO M-App: how to apply a rotation of this type?
	return Vector3(
		x.x * v.x + y.x * v.y + z.x * v.z,
		x.y * v.x + y.y * v.y + z.y * v.z,
		x.z * v.x + y.z * v.y + z.z * v.z
	);
}

// Rotations can be applied to versors or vectors just as well
Versor3 Matrix3::apply(Versor3 dir) const {
	return apply(dir.asVector()).asVersor();
}

Point3 Matrix3::apply(Point3 p) const {
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
Versor3 Matrix3::operator() (Versor3 p) { return apply(p); }
Point3  Matrix3::operator() (Point3 p) { return apply(p); }
Vector3 Matrix3::operator() (Vector3 p) { return apply(p); }

Versor3 Matrix3::axisX() const {
	return x;
}// TODO M-Ax a DONE
Versor3 Matrix3::axisY() const {
	return y;
}// TODO M-Ax b DONE
Versor3 Matrix3::axisZ() const {
	return z;
}// TODO M-Ax c DONE

Matrix3 Matrix3::operator+(Matrix3 m) const {
	return Matrix3(
		x.asVector() + m.x.asVector(),
		y.asVector() + m.y.asVector(),
		z.asVector() + m.z.asVector()
	);
}

// combine two rotations (r goes first!)
Matrix3 Matrix3::operator*(Matrix3 r) const {
	return Matrix3(
		x.x * r.x.x + x.y * r.y.x + x.z * r.z.x,
		x.x * r.x.y + x.y * r.y.y + x.z * r.z.y,
		x.x * r.x.z + x.y * r.y.z + x.z * r.z.z,

		y.x * r.x.x + y.y * r.y.x + y.z * r.z.x,
		y.x * r.x.y + y.y * r.y.y + y.z * r.z.y,
		y.x * r.x.z + y.y * r.y.z + y.z * r.z.z,

		z.x * r.x.x + z.y * r.y.x + z.z * r.z.x,
		z.x * r.x.y + z.y * r.y.y + z.z * r.z.y,
		z.x * r.x.z + z.y * r.y.z + z.z * r.z.z
	);
	/*
		r.x.x * x.x + r.y.x * x.y + r.z.x * x.z,
		r.x.x * y.x + r.y.x * y.y + r.z.x * y.z,
		r.x.x * z.x + r.y.x * z.y + r.z.x * z.z,

		r.x.y * x.x + r.y.y * x.y + r.z.y * x.z,
		r.x.y * y.x + r.y.y * y.y + r.z.y * y.z,
		r.x.y * z.x + r.y.y * z.y + r.z.y * z.z,

		r.x.z * x.z + r.y.z * x.y + r.z.z * x.z,
		r.x.z * y.z + r.y.z * y.y + r.z.z * y.z,
		r.x.z * z.z + r.y.z * z.y + r.z.z * z.z
	*/
}

Matrix3 Matrix3::operator*(Scalar scale) const {
	return Matrix3(
		x * scale,
		y * scale,
		z * scale
	);
}

Scalar Matrix3::det() const {
	return dot(cross(x, y), z.asVector());
}

Matrix3 Matrix3::inverse() const {
	// TODO M-Inv a DONE
	return transpose();
}

void Matrix3::invert() {
	// TODO M-Inv b DONE
	*this = transpose();
}

Matrix3 Matrix3::transpose() const {
	return Matrix3(
		x.x, x.y, x.z,
		y.x, y.y, y.z,
		z.x, z.y, z.z);
}

void Matrix3::transposed() {
	*this = transpose();
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
Matrix3 Matrix3::lookAt(Point3 eye, Point3 target, Versor3 up) {
	Matrix3 m;
	m.z = normalize(target - eye);
	m.x = normalize(cross(up, m.z));
	m.y = normalize(cross(m.z, m.x));
	return m;
}

// returns a rotation
Matrix3 Matrix3::toFrom(Versor3 to, Versor3 from) {
	// TODO M-ToFrom DONE
	
	//applicazione senza axis_angle (non funziona)
	/*
	Vector3 v = cross(from, to);
	Scalar s = norm(v);
	Scalar c = dot(from, to);
	Matrix3 id = Matrix3();
	Matrix3 vx = Matrix3(
		0, v.z, -v.y,
		-v.y, 0, -v.x,
		v.y, -v.x, 0
	);
	Matrix3 res = id + vx + vx * vx * (1 / (1 + c));
	return res;
	*/
	return Matrix3::from(AxisAngle::toFrom(to, from));
}

Matrix3 Matrix3::toFrom(Vector3 to, Vector3 from) {    // TODO M-ToFrom DONE
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
Matrix3 Matrix3::from(Quaternion q) {
	Matrix3 m = Matrix3(
		1.0 - 2.0 * (q.xyz.y * q.xyz.y) - 2.0 * (q.xyz.z * q.xyz.z),
		2.0 * (q.xyz.x * q.xyz.y) - 2.0 * (q.w * q.xyz.z),
		2.0 * (q.xyz.x * q.xyz.z) + 2.0 * (q.w * q.xyz.y),
		2.0 * (q.xyz.x * q.xyz.y) + 2.0 * (q.w * q.xyz.z),
		1.0 - 2.0 * (q.xyz.x * q.xyz.x) - 2.0 * (q.xyz.z * q.xyz.z),
		2.0 * (q.xyz.y * q.xyz.z) - 2.0 * (q.w * q.xyz.x),
		2.0 * (q.xyz.x * q.xyz.z) - 2.0 * (q.w * q.xyz.y),
		2.0 * (q.xyz.y * q.xyz.z) + 2.0 * (q.w * q.xyz.x),
		1.0 - 2.0 * (q.xyz.x * q.xyz.x) - 2.0 * (q.xyz.y * q.xyz.y)
	);
	return m;
	/*
	return Matrix3(
		1.0 - 2.0 * (q.xyz.y * q.xyz.y) - 2.0 * (q.xyz.z * q.xyz.z),
		2.0 * (q.xyz.x * q.xyz.y) - 2.0 * (q.w * q.xyz.z),
		2.0 * (q.xyz.x * q.xyz.z) + 2.0 * (q.w * q.xyz.y),

		2.0 * (q.xyz.x * q.xyz.y) + 2.0 * (q.w * q.xyz.z),
		1.0 - 2.0 * (q.xyz.x * q.xyz.x) - 2.0 * (q.xyz.z * q.xyz.z),
		2.0 * (q.xyz.y * q.xyz.z) - 2.0 * (q.w * q.xyz.x),

		2.0 * (q.xyz.x * q.xyz.z) - 2.0 * (q.w * q.xyz.y),
		2.0 * (q.xyz.y * q.xyz.z) + 2.0 * (q.w * q.xyz.x),
		1.0 - 2.0 * (q.xyz.x * q.xyz.x) - 2.0 * (q.xyz.y * q.xyz.y)
	);
	*/
}// TODO Q2M DONE

Matrix3 Matrix3::from(Euler e) {
	Matrix3 rotX = rotationX(e.v.x);
	Matrix3 rotY = rotationY(e.v.y);
	Matrix3 rotZ = rotationZ(e.v.z);
	return rotZ * rotX * rotY;
}// TODO E2M DONE

Matrix3 Matrix3::from(AxisAngle aa) {
	Matrix3 m;
	Scalar aaCos = cos(aa.angle);
	Scalar aaSin = sin(aa.angle);
	Scalar offset = 1.0 - aaCos;

	m.x.x = aaCos + (aa.axis.x * aa.axis.x * offset);
	m.y.y = aaCos + (aa.axis.y * aa.axis.y * offset);
	m.z.z = aaCos + (aa.axis.z * aa.axis.z * offset);

	Scalar tmp1 = aa.axis.x * aa.axis.y * offset;
	Scalar tmp2 = aa.axis.z * aaSin;
	m.x.y = tmp1 + tmp2;
	m.y.x = tmp1 - tmp2;

	tmp1 = aa.axis.x * aa.axis.z * offset;
	tmp2 = aa.axis.y * aaSin;
	m.x.z = tmp1 - tmp2;
	m.z.x = tmp1 + tmp2;

	tmp1 = aa.axis.y * aa.axis.z * offset;
	tmp2 = aa.axis.x * aaSin;
	m.y.z = tmp1 + tmp2;
	m.z.y = tmp1 - tmp2;

	return m;
}// TODO A2M DONE

// does this Matrix3 encode a rotation?
bool Matrix3::isRot() const {
	// TODO M-isR    // TODO M-isR DONE
	return abs(det() - 1) < EPSILON
		&& (cross(x, y) == z.asVector())
		&& (cross(y, z) == x.asVector())
		&& (cross(z, x) == y.asVector());
}

// return a rotation matrix around an axis
Matrix3 Matrix3::rotationX(Scalar angleDeg) {
	Scalar angleRad = angleDeg * DegToRad;
	return Matrix3(
		1,        0,             0,
		0,  cos(angleRad), sin(angleRad),
		0, -sin(angleRad), cos(angleRad));
}// TODO M-Rx DONE

Matrix3 Matrix3::rotationY(Scalar angleDeg) {
	Scalar angleRad = angleDeg * DegToRad;
	return Matrix3(
		cos(angleRad),  0, sin(angleRad),
		       0,       1,       0,
		-sin(angleRad), 0, cos(angleRad));
}// TODO M-Ry DONE

Matrix3 Matrix3::rotationZ(Scalar angleDeg) {
	Scalar angleRad = angleDeg * DegToRad;
	return Matrix3(
		cos(angleRad),  sin(angleRad), 0,
		-sin(angleRad), cos(angleRad), 0,
		      0,              0,       1);
}// TODO M-Rz DONE

void Matrix3::printf() const {
	std::cout << "(";
	x.printf();
	std::cout << ",";
	y.printf();
	std::cout << ",";
	z.printf();
	std::cout << ")";
} // TODO Print