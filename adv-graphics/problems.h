#pragma once

#include "vector3.h"
#include "point3.h"
#include "versor3.h"
#include "axis_angle.h"
#include "euler.h"
#include "quaternion.h"
#include "matrix3.h"
#include "transform.h"

// The BIG CHOICE:
//typedef Quaternion Rotation;
//typedef AxisAngle Rotation;
//typedef Matrix3 Rotation;
//typedef Euler Rotation;

// POD -- Plain Old Data
struct Ray{
    Point3 p;
    Vector3 v;
};

struct Sphere{
    Point3 c;
    Scalar r;
};

struct Plane{
    Vector3 w, h;
    Point3 o;
};

const Point3 NO_INTERSECTION(666,666,666);

Point3 intersect(const Ray& r, const Sphere& s);

Point3 intersect(const Ray& r, const Plane& p);

// does a eye in a given position sees the target, given the angle cone and maxDist?
bool isSeen(const Vector3& ViewDir, const Point3& eye, const Point3& target, Scalar angleDeg, Scalar maxDist);

// returns the reflected direction of something bouncing in n
Versor3 reflect(const Versor3& d, const Versor3& n);

bool areCoplanar(const Versor3& a, const Versor3& b, const Versor3& c);

// normal of a plane, given three points
Versor3 planeNormal(const Point3& a, const Point3& b, const Point3& c);

// return a versor as similar as possible to a, but ortogonal to b
Versor3 orthogonalize(const Versor3& a, const Versor3& b);

// a bullet is in position pa and has velocity va
// a target is in position pb and has velocity vb
// returns the position of the target in the moment it is closest to the bullet
Point3 hitPos(const Point3& pa, const Vector3& va, const Point3& pb, const Vector3& vb);