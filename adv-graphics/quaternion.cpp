#pragma once

#include <math.h>
#include "quaternion.h"
#include "euler.h"
#include "axis_angle.h"
#include "matrix3.h"

Quaternion::Quaternion(Scalar x, Scalar y, Scalar z, Scalar w):Quaternion(Vector3(x, y, z), w) { }
Quaternion::Quaternion(Vector3 xyz, Scalar w) :xyz(xyz), w(w) { }
Quaternion::Quaternion(Versor3 xyz, Scalar w) :xyz(xyz.asVector()), w(w) { }
Quaternion::Quaternion(Point3 xyz, Scalar w) : xyz(xyz.asVector()), w(w) { }
Quaternion::Quaternion(Scalar x, Scalar y, Scalar z) : Quaternion(Vector3(x, y, z), 0) { }
Quaternion::Quaternion(Vector3 xyz) : xyz(xyz), w(0) { }
Quaternion::Quaternion(Versor3 xyz) : xyz(xyz.asVector()), w(0) { }
Quaternion::Quaternion(Point3 xyz) : xyz(xyz.asVector()), w(0) { }
Quaternion::Quaternion() : Quaternion(0, 0, 0, 1) { }

Quaternion::Quaternion(const Point3& p):Quaternion(p.x, p.y, p.z, 0) { }

Vector3 Quaternion::apply(Vector3  v) const {
	// TODO Q-App: how to apply a rotation of this type? DONE
	return Matrix3::from(*this).apply(v);
	//return (*this * Quaternion(v.x, v.y, v.z, 0) * -conjugated()).xyz;
}

// Rotations can be applied to versors or vectors just as well DONE
Versor3 Quaternion::apply(Versor3 dir) const {
	return apply(dir.asVector()).asVersor();
}

Point3 Quaternion::apply(Point3 p) const {
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synomim of "R.apply( p )" DONE
Versor3 Quaternion::operator() (Versor3 p) { return Quaternion::apply(p); }
Point3 Quaternion::operator() (Point3 p) { return Quaternion::apply(p); }
Vector3 Quaternion::operator() (Vector3 p) { return Quaternion::apply(p); }

Versor3 Quaternion::axisX() const {
	return inverse().apply(Versor3::right());
};  // TODO Q-Ax a DONE

Versor3 Quaternion::axisY() const {
	return inverse().apply(Versor3::up());
};  // TODO Q-Ax b DONE

Versor3 Quaternion::axisZ() const {
	return inverse().apply(Versor3::forward());
};  // TODO Q-Ax c DONE

Quaternion Quaternion::operator+(Quaternion q) const {
	return Quaternion(xyz + q.xyz, w + q.w);
}

Quaternion Quaternion::operator-(Quaternion q) const {
	return Quaternion(xyz - q.xyz, w - q.w);
}

Quaternion Quaternion::operator-() const {
	return Quaternion(-xyz, -w);
}

Quaternion Quaternion::operator*(Scalar scale) const {
	return Quaternion(xyz*scale, w*scale);
}

// conjugate DONE
Quaternion Quaternion::operator*(Quaternion q) const {
	return Quaternion(
		xyz * q.w + q.xyz * w + cross(xyz, q.xyz),
		w * q.w - dot(xyz, q.xyz));
}

Quaternion Quaternion::operator/(Scalar scale) const {
	return Quaternion(
		xyz.x / scale,
		xyz.y / scale,
		xyz.z / scale,
		w / scale);
}

bool Quaternion::operator==(Quaternion other) const {
	return xyz.x >= other.xyz.x - EPSILON && xyz.x <= other.xyz.x + EPSILON
		&& xyz.y >= other.xyz.y - EPSILON && xyz.y <= other.xyz.y + EPSILON
		&& xyz.z >= other.xyz.z - EPSILON && xyz.z <= other.xyz.z + EPSILON
		&& w >= other.w - EPSILON && w <= other.w + EPSILON;
}

Quaternion Quaternion::inverse() const {
	// TODO Q-Inv a DONE
	return conjugated();
}

//TODO: rimosso const{, chiedere se va bane
void Quaternion::invert() {
	conjugate();
}

// specific methods for quaternions... DONE
Quaternion Quaternion::conjugated() const {
	return Quaternion(-xyz, w);
}

void Quaternion::conjugate() {
	xyz = -xyz;
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
Quaternion Quaternion::lookAt(Point3 eye, Point3 target, Versor3 up) {
	return Quaternion(from(Matrix3::lookAt(eye, target, up)));
}

// returns a rotation DONE
Quaternion Quaternion::toFrom(Versor3 to, Versor3 from) {
	return Quaternion::from(AxisAngle::toFrom(to, from));
}

Quaternion Quaternion::toFrom(Vector3 to, Vector3 from) {
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
Quaternion Quaternion::from(Matrix3 m) {
	return Quaternion(from(AxisAngle::from(m)));
}// TODO M2Q DONE

Quaternion Quaternion::from(Euler e) {
	Scalar sinX = sin(e.v.x * 0.5);
	Scalar cosX = cos(e.v.x * 0.5);
	Scalar sinY = sin(e.v.y * 0.5);
	Scalar cosY = cos(e.v.y * 0.5);
	Scalar sinZ = sin(e.v.z * 0.5);
	Scalar cosZ = cos(e.v.z * 0.5);

	return Quaternion(
		sinX * cosY * cosZ - cosX * sinY * sinZ,
		cosX * sinY * cosZ + sinX * cosY * sinZ,
		cosX * cosY * sinZ - sinX * sinY * cosZ,
		cosX * cosY * cosZ + sinX * sinY * sinZ
	);
}// TODO E2Q DONE

Quaternion Quaternion::from(AxisAngle a) {
	return Quaternion(a.axis * sin(a.angle / 2.0), -cos(a.angle / 2.0));
}// TODO A2Q DONE

Scalar Quaternion::getX() const { return xyz.x; }
Scalar Quaternion::getY() const { return xyz.y; }
Scalar Quaternion::getZ() const { return xyz.z; }
Scalar Quaternion::getW() const { return w; }

// does this quaternion encode a rotation? DONE
bool Quaternion::isRot() const {
	Scalar n = squaredNorm(*this);
	return n > 1 - EPSILON && n < 1 + EPSILON;
}


// does this quaternion encode a point? DONE
bool Quaternion::isPoint() const {
	return w <= EPSILON;
}

bool Quaternion::isEqual(const Quaternion& other) {
	return *this == other;
}

Quaternion Quaternion::rotationX(Scalar angleDeg)
{
	return from(AxisAngle::rotationX(angleDeg));
}

Quaternion Quaternion::rotationY(Scalar angleDeg)
{
	return from(AxisAngle::rotationY(angleDeg));
}

Quaternion Quaternion::rotationZ(Scalar angleDeg)
{
	return from(AxisAngle::rotationZ(angleDeg));
}

void Quaternion::printf() const {
	std::cout << "(" << xyz.x <<
		", " << xyz.y <<
		", " << xyz.z <<
		", " << w << ")";
}