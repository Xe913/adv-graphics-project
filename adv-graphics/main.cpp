#pragma once

#include <iostream>
#include <math.h>
#include "transform.h"

using namespace std;

void check(bool condition, std::string descr ){
    std::cout << "Test " << descr << ": "
              << ((condition)?"PASSED":"FAILED" )
              << "\n";
}

void unitTestNormalize() {
    Versor3 expected = Versor3::right();
	Versor3 converted = normalize(Vector3(2, 0, 0));
    check(converted == expected, "CONVERT");
}

void unitTestToFromQuat() {
	Versor3 to = Versor3::right();
	Versor3 from = Versor3::right();
    Quaternion expected = Quaternion();
    Quaternion q = Quaternion::toFrom(to, from);
	check(q == expected, "TOFROM");
}

void unitTestInverse(){
    Transform t;
    t.translation = Vector3(2,0,0);
    t.scale = 3;

    Point3 p0,q,p1;

    q = t.apply(p0);

    Transform ti = t.inverse();
    p1 = ti.apply(q);

    check( p0 == p1 , "INVERSE");
}

void unitTestCumulate() {
	Transform t1, t2;
	t1.translation = Vector3(2, 0, 0);
	t1.scale = 4;
	t1.rotation = Quaternion::from(Euler(90, 0, 0));

	t2.translation = Vector3(4, -3, 1);
	t2.scale = 0.4;
	t2.rotation = Quaternion::from(Euler(90, 0, 0));

	Point3 p(3, 1, 3);
	Point3 q0 = t2.apply(t1.apply(p));
	Transform product = t2 * t1;
	Point3 q1 = product.apply(p);

	check(q0.isEqual(q1), "CUMULATE");
}

void unitTestIsRot() {
	Quaternion qPoint(Point3::origin().asVector(), 0);
	Quaternion qRot;

	check(qPoint.isRot() == false, "QUATERNION NOT ROT");
	check(qRot.isRot() == true, "QUATERNION ROT");

	AxisAngle aRot(Versor3::forward(), 30.0 * DegToRad);

	check(aRot.isRot() == true, "AXIS_ANGLE ROT");

	Matrix3 mNotRot(
		0, 1, 0,
		0, 1, 0,
		0, 1, 0
	);
	Matrix3 mRot;

	check(mNotRot.isRot() == false, "MATRIX3 NOT ROT");
	check(mRot.isRot() == true, "MATRIX3 ROT");

	Euler eRot(0, 30, 0);

	check(eRot.isRot() == true, "EULER ROT");
}

void unitTestMatrixRotation() {
	Matrix3 x1 = Matrix3::rotationX(90);
	Euler ex1 = Euler::from(x1);
	Matrix3 x2 = Matrix3::rotationX(90);
	Matrix3 res = x1 * x2;
	Euler::from(res).printf();
}

void unitTestConversion() {
    AxisAngle aa = AxisAngle();
	Versor3 noConversion = aa.apply(Versor3::up());
	Versor3 viaEuler = Euler::from(aa).apply(Versor3::up());
	Versor3 viaQuaternion = Quaternion::from(aa).apply(Versor3::up());
	Versor3 viaMatrix3 = Matrix3::from(aa).apply(Versor3::up());
	check(noConversion == viaEuler, "CONVERSION FROM AXIS_ANGLE TO EULER");
	check(noConversion == viaQuaternion, "CONVERSION FROM AXIS_ANGLE TO QUATERNION");
	check(noConversion == viaMatrix3, "CONVERSION FROM AXIS_ANGLE TO MATRIX3");
}

void unitTestToFrom() {
    Versor3 from = Versor3::left();
	Versor3 to = Versor3::down();

    Euler etofrom = Euler::toFrom(to, from);
	Versor3 e = Euler::toFrom(to, from).apply(from);
	Versor3 aa = AxisAngle::toFrom(to, from).apply(from);
	Versor3 q = Quaternion::toFrom(to, from).apply(from);
	Versor3 m = Matrix3::toFrom(to, from).apply(from);

    check(e == to, "TOFROM EULER");
	check(aa == to, "TOFROM AXIS_ANGLE");
	check(q == to, "TOFROM QUATERNION");
	check(m == to, "TOFROM MATRIX3");
}

int main()
{
    unitTestNormalize();
    unitTestInverse();
	unitTestCumulate();
    unitTestIsRot();
	unitTestMatrixRotation();
    unitTestConversion();
	unitTestToFrom();
    return 0;
}
