#pragma once

#include "problems.h"

Point3 intersect(const Ray& r, const Sphere& s) {
	Scalar a = dot(r.v, r.v);
	Scalar b = 2 * dot(r.p - s.c, r.v);
	Scalar c = dot(r.p - s.c, r.p - s.c) - s.r * s.r;

	// euqation is: a*k^2 + b*k + c == 0

	Scalar delta = b * b - 4 * a * c;
	if (delta < 0) return NO_INTERSECTION;

	Scalar k = (-b - sqrt(delta)) / (2 * a);
	if (k < 0) return NO_INTERSECTION;
	return r.p + k * r.v;
}

Point3 intersect(const Ray& r, const Plane& p) {
	// TODO: P-inter-plane DONE
	Vector3 pNorm = cross(p.w, p.h);
	Scalar div = dot(r.v, pNorm);
	if (abs(div) > EPSILON) {
		Scalar distance = dot(-(r.p - p.o), pNorm) / div;
		return r.p + (r.v * distance);
	}
	return NO_INTERSECTION;
}

// does a eye in a given position sees the target, given the angle cone and maxDist?
bool isSeen(const Vector3& ViewDir, const Point3& eye, const Point3& target, Scalar angleDeg, Scalar maxDist) {
	Vector3 dirVector = target - eye;
	if (squaredNorm(dirVector) > maxDist * maxDist) return false;
	return dot(normalize(dirVector), ViewDir) > cos((angleDeg * DegToRad) / 2.0);
	// TODO: isSeen DONE
}

// returns the reflected direction of something bouncing in n
Versor3 reflect(const Versor3& d, const Versor3& n) {
	// TODO: P-reflect DONE
	return normalize(d.asVector() - 2 * dot(d, n) * n);
}

bool areCoplanar(const Versor3& a, const Versor3& b, const Versor3& c) {
	// TODO: P-coplanar DONE
	return dot(a, b) <= EPSILON && dot(a, c) <= EPSILON;
}

// normal of a plane, given three points
Versor3 planeNormal(const Point3& a, const Point3& b, const Point3& c) {
	// TODO: P-palneNorm DONE
	return normalize(cross(b - a, c - a));
}

// return a versor as similar as possible to a, but ortogonal to b
Versor3 orthogonalize(const Versor3& a, const Versor3& b) {
	// TODO: P-ortogonalize
	return normalize(cross(b, normalize(cross(a, b))));
}

// a bullet is in position pa and has velocity va
// a target is in position pb and has velocity vb
// returns the position of the target in the moment it is closest to the bullet
Point3 hitPos(const Point3& pa, const Vector3& va, const Point3& pb, const Vector3& vb) {
	// TODO: P-hitPos DONE
	return pb + vb * (-dot(pb - pa, vb - va) / squaredNorm(vb - va)); //pos(t) = init_pos + vel * t
}