#pragma once

#include <math.h>
#include "vector3.h"
#include "point3.h"
#include "versor3.h"

/* Quaternion class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

class Euler;
class AxisAngle;
class Matrix3;

class Quaternion{
public:
	Vector3 xyz;
	Scalar w;

	Quaternion(Scalar x, Scalar y, Scalar z, Scalar w);
	Quaternion(Vector3 xyzV, Scalar w);
	Quaternion(Versor3 xyzVs, Scalar w);
	Quaternion(Point3 xyzP, Scalar w);
    Quaternion(Scalar x, Scalar y, Scalar z);
	Quaternion(Vector3 xyz);
	Quaternion(Versor3 xyz);
	Quaternion(Point3 xyz);
    Quaternion();
    Quaternion(const Point3& p);

    Vector3 apply( Vector3  v) const;
    Versor3 apply( Versor3 dir ) const;
    Point3 apply( Point3 p ) const;

    Versor3 operator() (Versor3 p);
    Point3  operator() (Point3 p);
    Vector3 operator() (Vector3 p);

    Versor3 axisX() const;

	Versor3 axisY() const;

	Versor3 axisZ() const;

	Quaternion operator+(Quaternion q) const;
	Quaternion operator-(Quaternion q) const;
	Quaternion operator-() const;
    Quaternion operator*(Scalar scale) const;
	Quaternion operator*(Quaternion q) const;
	Quaternion operator/(Scalar scale) const;
    bool operator==(Quaternion other) const;

    Quaternion inverse() const;

    void invert();

    Quaternion conjugated() const;

	void conjugate();

    static Quaternion lookAt( Point3 eye, Point3 target, Versor3 up = Versor3::up() );

    static Quaternion toFrom( Versor3 to, Versor3 from );

    static Quaternion toFrom( Vector3 to, Vector3 from );

    static Quaternion from( Matrix3 m );
    static Quaternion from( Euler e );
    static Quaternion from( AxisAngle e );

	Scalar getX() const;
	Scalar getY() const;
	Scalar getZ() const;
	Scalar getW() const;

    // does this quaternion encode a rotation? DONE
    bool isRot() const;


    // does this quaternion encode a point? DONE
    bool isPoint() const;

	bool isEqual(const Quaternion& other);

	static Quaternion rotationX(Scalar angleDeg);
	static Quaternion rotationY(Scalar angleDeg);
	static Quaternion rotationZ(Scalar angleDeg);

    void printf() const;
};

inline Quaternion operator*(Scalar scale, Quaternion q) {
    return q * scale;
}

inline Scalar dot(const Quaternion& a, const Quaternion& b) {
	return dot(a.xyz, b.xyz) + a.w * b.w;
}

inline Scalar squaredNorm(const Quaternion& q) {
    return dot(q, q);
}

inline Scalar norm(const Quaternion& q) {
	return sqrt(squaredNorm(q));
}

inline Quaternion normalize(const Quaternion& q) {
    return q / norm(q);
}

// interpolation or rotations
inline Quaternion lerp(const Quaternion& a, const Quaternion& b, Scalar t) {
    return a * (1 - t) + ((dot(a, b) > 0) ? b : -b) * t;
}

inline Quaternion slerp(const Quaternion& a, const Quaternion& b, Scalar t) {
	Scalar dotProd = dot(a, b);
    Quaternion _b = dotProd >= 0 ? b : b.inverse();
    Scalar theta = acos(dot(a, _b));
    return normalize(a * (sin(theta * (1 - t)) / sin(theta)) + b * (sin(theta * t) / sin(theta)));
}