#pragma once
#include <iostream>
#include <math.h>
#include "vector3.h"
#include "point3.h"
#include "assert.h"

typedef double Scalar;

class Versor3 {
    Versor3(Scalar _x, Scalar _y, Scalar _z);
public:
    Scalar x, y, z;

    static Versor3 right();
    static Versor3 left();
    static Versor3 up();
    static Versor3 down();
    static Versor3 forward();
    static Versor3 backward();

    Scalar& operator[](int i);

    Scalar operator[](int i) const;

    Vector3 operator*(Scalar k) const;

    Versor3 operator-() const;

	Versor3 operator+() const;

	bool operator==(Versor3& other) const;

    friend Versor3 normalize(Vector3 p);
	friend Versor3 Vector3::asVersor() const;
	friend Versor3 Point3::asVersor() const;

    Vector3 asVector() const;

    Point3 asPoint() const;

	bool isEqual(Versor3 other);

	void printf() const;
};

inline Versor3 normalize(Vector3 p){
    Scalar n = norm(p);
    if (n >= 0 + EPSILON2) p = p / n;
    return Versor3(p.asVersor());
}

// cosine between a and b
inline Scalar dot(const Versor3 &a, const Versor3 &b){
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

// length of projection of b along a
inline Scalar dot(const Versor3 &a, const Vector3 &b){
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

inline Vector3 cross(const Versor3 &a, const Versor3 &b){
    return Vector3(
        a.y*b.z-a.z*b.y,
        a.z*b.x-a.x*b.z,
        a.x*b.y-a.y*b.x);
}

inline Vector3 operator*( Scalar k, const Versor3& a){ return a * k; }

inline Versor3 nlerp(const Versor3& a, const Versor3& b, Scalar t) {
	return normalize((1 - t) * a + t * b);
}

inline Versor3 slerp(const Versor3& a, const Versor3& b, Scalar t) {
	Scalar theta = acos(dot(a, b));
	return normalize(a * (sin(theta * (1 - t)) / sin(theta)) + b * (sin(theta * t) / sin(theta)));
}

// under my own responsability, I declare this vector to be unitary and therefore a VERSOR
inline Versor3 Vector3::asVersor() const {
    Scalar squared = squaredNorm(*this) - 1.0;
    assert(squaredNorm(*this) - 1.0 <= EPSILON2);
    return Versor3(x, y, z);
}

inline Versor3 Point3::asVersor() const {
	return Point3(x, y, z).asVector().asVersor();
}