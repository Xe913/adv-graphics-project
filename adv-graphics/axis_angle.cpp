#pragma once

#include <math.h>
#include "quaternion.h"
#include "euler.h"
#include "axis_angle.h"
#include "matrix3.h"

// TODO A-Ide: this constructor construct the identity rotation
AxisAngle::AxisAngle():axis(Versor3::forward()), angle(0) {}

// TODO A-FromPoint
// returns a AxisAngle encoding a point

AxisAngle::AxisAngle(const Versor3& axis) : axis(axis), angle(0) { }
AxisAngle::AxisAngle(const Vector3& axis) : axis(normalize(axis)), angle(0) { }
AxisAngle::AxisAngle(const Point3& axis):axis(normalize(axis.asVector())), angle(0) { }

AxisAngle::AxisAngle(const Versor3& axis, Scalar angle) : axis(axis), angle(angle) {
	if (angle < 0) angle = 0;
	if (angle > 2) angle = 2;
}

AxisAngle::AxisAngle(const Vector3& axis, Scalar angle) : AxisAngle(normalize(axis), angle) { }
AxisAngle::AxisAngle(const Point3& axis, Scalar angle) : AxisAngle((normalize(axis.asVector())), angle) { }


Vector3 AxisAngle::apply(Vector3 v) const {
	// TODO A-App: how to apply a rotation of this type? DONE
	return Vector3(Matrix3::from(*this).apply(v));
}

// Rotations can be applied to versors or vectors just as well
Versor3 AxisAngle::apply(Versor3 dir) const {
	return apply(dir.asVector()).asVersor();
}

Point3 AxisAngle::apply(Point3 p) const {
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
Versor3 AxisAngle::operator() (Versor3 p) { return apply(p); }
Point3 AxisAngle::operator() (Point3 p) { return apply(p); }
Vector3 AxisAngle::operator() (Vector3 p) { return apply(p); }

Versor3 AxisAngle::axisX() const {
	return Matrix3::from(*this).axisX();
}// TODO A-Ax a DONE
Versor3 AxisAngle::axisY() const {
	return Matrix3::from(*this).axisY();
}// TODO A-Ax b DONE
Versor3 AxisAngle::axisZ() const {
	return axis;
}// TODO A-Ax c DONE

// conjugate
AxisAngle AxisAngle::operator * (AxisAngle r) const {
	return from(Matrix3::from(*this) * Matrix3::from(r));
}

AxisAngle AxisAngle::inverse() const {
	// TODO A-Inv a
	return AxisAngle(axis, -angle);
}

void AxisAngle::invert() {
	angle = -angle;
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
AxisAngle AxisAngle::lookAt(Point3 eye, Point3 target, Versor3 up) {
	return AxisAngle::from(Matrix3::lookAt(eye, target, up));
}

// returns a rotation
AxisAngle AxisAngle::toFrom(Versor3 to, Versor3 from) {
	Vector3 rotationAxis = cross(from, to);
	return AxisAngle(
		normalize(rotationAxis),
		atan2(norm(rotationAxis), dot(from, to))
	);
}

AxisAngle AxisAngle::toFrom(Vector3 to, Vector3 from) {
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
AxisAngle AxisAngle::from(Matrix3 m) {
	Versor3 axis = normalize(Vector3(m.x.z - m.z.x, m.y.x - m.x.y, m.z.y - m.y.z));
	Scalar angle = acos((m.x.x + m.y.y, +m.z.z - 1) / 2);
	return AxisAngle(axis, angle);
}// TODO M2A DONE

AxisAngle AxisAngle::from(Euler e) {
	return from(Matrix3::from(e));
}// TODO E2A

AxisAngle AxisAngle::from(Quaternion q) {
	if (q.w > 1) q = normalize(q);
	return AxisAngle(normalize(q.xyz), 2.0 * acos(q.w));
}// TODO Q2A DONE

bool AxisAngle::isRot() const { return true; }

// does this AxisAngle encode a point? DONE
bool AxisAngle::isPoint() const {
	return angle <= EPSILON;
}

AxisAngle AxisAngle::rotationX(Scalar angleDeg) //TODO A-Rx DONE
{
	return AxisAngle(Versor3::right(), angleDeg * DegToRad);
}

AxisAngle AxisAngle::rotationY(Scalar angleDeg) //TODO A-Ry DONE
{
	return AxisAngle(Versor3::up(), angleDeg * DegToRad);
}

AxisAngle AxisAngle::rotationZ(Scalar angleDeg) //TODO : A-Rz DONE
{
	return AxisAngle(Versor3::forward(), angleDeg * DegToRad);
}

void AxisAngle::printf() const {
	axis.printf();
	std::cout << ", " << angle;
} // TODO Print
