#pragma once

#include <math.h>
#include "quaternion.h"
#include "euler.h"
#include "axis_angle.h"
#include "matrix3.h"

Euler::Euler() : v() { }
Euler::Euler(Vector3 _v) : v(_v) { }
Euler::Euler(Versor3 _vs) : v(_vs.asVector()) { }
Euler::Euler(Point3 _p) : v(_p.asVector()) { }
Euler::Euler(Scalar x, Scalar y, Scalar z) : v(x, y, z) { }
Euler::Euler(Scalar m00, Scalar m01, Scalar m02,
	Scalar m10, Scalar m11, Scalar m12,
	Scalar m20, Scalar m21, Scalar m22) {
	Matrix3 m = Matrix3(m00, m01, m02, m10, m11, m12, m20, m21, m22);
	*this = from(m);
}

Vector3 Euler::apply(Vector3  v) const {
	// TODO E-App: how to apply a rotation of this type?
	return Matrix3::from(*this).apply(v);
}

// Rotations can be applied to versors or vectors just as well
Versor3 Euler::apply(Versor3 dir) const {
	return apply(dir.asVector()).asVersor();
}

Point3 Euler::apply(Point3 p) const {
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
Versor3 Euler::operator()(Versor3 p) { return Euler::apply(p); }
Point3 Euler::operator()(Point3  p) { return Euler::apply(p); }
Vector3 Euler::operator()(Vector3 p) { return Euler::apply(p); }

Versor3 Euler::axisX() const {
	return Matrix3::from(*this).axisX();
}  // TODO E-Ax a
Versor3 Euler::axisY() const {
	return Matrix3::from(*this).axisY();
}// TODO E-Ax b
Versor3 Euler::axisZ() const {
	return Matrix3::from(*this).axisZ();
}// TODO E-Ax c

// conjugate
Euler Euler::operator*(Euler b) const {
	return from(Matrix3::from(*this) * Matrix3::from(b));
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
Euler Euler::lookAt(Point3 eye, Point3 target, Versor3 up) {
	// TODO E-LookAt DONE
	return from(Matrix3::lookAt(eye, target, up));
}

// returns a rotation
Euler Euler::toFrom(Versor3 to, Versor3 from) {
	// TODO E-ToFrom DONE
	return Euler::from(AxisAngle::toFrom(to, from));
}

Euler Euler::toFrom(Vector3 to, Vector3 from) {
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
Euler Euler::from(Quaternion q) {
	return from(Matrix3::from(q));
} // TODO Q2M

Euler Euler::from(Matrix3 m) {
	Euler e;
	if (-m.z.y <= -1) {
		e.v.x = -M_PI_2;
	}
	else {
		if (-m.z.y >= 1) {
			e.v.x = M_PI_2;
		}
		else {
			e.v.x = asin(-m.z.y);
		}
	}

	if (abs(m.z.y) > 1 - EPSILON) {
		e.v.y = atan2(-m.y.z, m.x.x);
		e.v.z = 0.0;
	}
	else {
		e.v.y = atan2(m.z.x, m.z.z);
		e.v.z = atan2(m.x.y, m.y.y);
	}
	e.v *= RadToDeg;
	return e.v;
} // TODO E2M DONE

Euler Euler::from(AxisAngle aa) {
	return from(Quaternion::from(aa));
} // TODO E2M

// does this Euler encode a rotation?
bool Euler::isRot() const { return true; }

// return a rotation matrix around an axis
Euler Euler::rotationX(Scalar angleDeg) {
	return Euler(angleDeg, 0, 0);
}// TODO E-Rx DONE
Euler Euler::rotationY(Scalar angleDeg) {
	return Euler(0, angleDeg, 0);
}// TODO E-Rx DONE
Euler Euler::rotationZ(Scalar angleDeg) {
	return Euler(0, 0, angleDeg);
}// TODO E-Rx DONE

Scalar Euler::GetX() const { return v.x; }
Scalar Euler::GetY() const { return v.y; }
Scalar Euler::GetZ() const { return v.z; }

void Euler::printf() const { v.printf(); }